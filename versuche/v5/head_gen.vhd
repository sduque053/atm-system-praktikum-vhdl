LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY head_gen IS
	PORT (	clk : IN std_logic;
		res : IN std_logic;
		set_head : IN std_logic;
		head_in : IN std_logic_vector(31 DOWNTO 0);
		output_head : IN std_logic;
		head_data : OUT std_logic_vector(7 DOWNTO 0);
		hg_done : OUT std_logic);
END head_gen;

ARCHITECTURE struct OF head_gen IS
   
signal head_intern : std_logic_vector(31 downto 0);
signal hg_cnt_latch_en, hg_cnt_en, hg_timed_en : std_logic;
signal hec_res, hec_enable, hg_done_s : std_logic;
signal hg_cnt_val : std_logic_vector(5 downto 0);
signal hec_in : std_logic := '1';
signal hec_val : std_logic_vector(7 downto 0);
signal out_cnt_val : std_logic_vector(2 downto 0);
   
    COMPONENT hec_gen
	PORT (data, clk, res, enab : IN std_logic;
	      hec : OUT std_logic_vector(7 DOWNTO 0));
    END COMPONENT;
    
    component my_dff_re
        PORT (d, clk, en, res : IN std_logic;
	          q, qb  : OUT std_logic);
    END component ;
    
    component counter
        generic( Bit_Size : integer := 8;
             Start : integer:= 0);
        Port ( clk : in STD_LOGIC;
               enab : in STD_LOGIC;
               res : in STD_LOGIC;
               target : in std_logic_vector(Bit_Size-1 downto 0);
               cnt_val : out STD_LOGIC_vector(Bit_Size-1 downto 0)
               );
    end component;

BEGIN 

GEN_HREG: for I in 0 to 31 generate

    H_REG: my_dff_re port map(
        head_in(I), clk, set_head, res,
        head_intern(I), open );
            
end generate GEN_HREG;

--Because the Counter enable doesn't stay as one for the duration of the calculation is neccessary to latch it
--for the 34 cycles and then unlatch it when it finishes
hg_cnt_latch_en<=set_head or hg_done_s;

HG_CNT_LATCH: my_dff_re PORT map (set_head, clk, hg_cnt_latch_en, res, hg_cnt_en, open);

hg_timed_en<=hg_cnt_en and (not hg_done_s);

HG_CNT: counter
        generic map ( Bit_Size=> 6 , Start=>1)
        port map ( clk , hg_timed_en, set_head, "100010", hg_cnt_val );
        
process(hg_cnt_val)
begin    
                  
    
end process;

hg_done<=hg_done_s;


process(hg_cnt_val, out_cnt_val)
begin

case hg_cnt_val is

    when "000000" => hec_res<='0';
    when "000001" => hec_res<='1';
    when "100010" => hec_res<='0';
    when "000010" => hec_res<='0';
    when "000011"=> hec_res<='0';
    when "000100" => hec_res<='0';
    when "000101" => hec_res<='0';
    when "000110" => hec_res<='0';
    when "000111" => hec_res<='0';
    when "001000" => hec_res<='0';
    when "001001" => hec_res<='0';
    when "001010" => hec_res<='0';
    when "001011" => hec_res<='0';
    when "001100" => hec_res<='0';
    when "001101" => hec_res<='0';
    when "001110" => hec_res<='0';
    when "001111" => hec_res<='0';
    when "010000" => hec_res<='0';
    when "010001" => hec_res<='0';
    when "010010" => hec_res<='0';
    when "010011" => hec_res<='0';
    when "010100" => hec_res<='0';
    when "010101" => hec_res<='0';
    when "010110" => hec_res<='0';
    when "010111" => hec_res<='0';
    when "011000" => hec_res<='0';
    when "011001" => hec_res<='0';
    when "011010" => hec_res<='0';
    when "011011" => hec_res<='0';
    when "011100" => hec_res<='0';
    when "011101" => hec_res<='0';
    when "011110" => hec_res<='0';
    when "011111" => hec_res<='0';
    when "100000" => hec_res<='0';
    when "100001" => hec_res<='0';
    when others => hec_res<='-';
            
end case;
    
case hg_cnt_val is

    when "000000" => hec_enable<='0';
    when "000001" => hec_enable<='0';
    when "100010" => hec_enable<='0';
    when "000010" => hec_enable<='1';
    when "000011" => hec_enable<='1';
    when "000100"  => hec_enable<='1';
    when "000101"  => hec_enable<='1';
    when "000110" => hec_enable<='1';
    when "000111" => hec_enable<='1';
    when "001000" => hec_enable<='1';
    when "001001" => hec_enable<='1';
    when "001010" => hec_enable<='1';
    when "001011" => hec_enable<='1';
    when "001100" => hec_enable<='1';
    when "001101" => hec_enable<='1';
    when "001110" => hec_enable<='1';
    when "001111" => hec_enable<='1';
    when "010000" => hec_enable<='1';
    when "010001" => hec_enable<='1';
    when "010010" => hec_enable<='1';
    when "010011" => hec_enable<='1';
    when "010100" => hec_enable<='1';
    when "010101" => hec_enable<='1';
    when "010110" => hec_enable<='1';
    when "010111" => hec_enable<='1';
    when "011000" => hec_enable<='1';
    when "011001" => hec_enable<='1';
    when "011010" => hec_enable<='1';
    when "011011" => hec_enable<='1';
    when "011100" => hec_enable<='1';
    when "011101" => hec_enable<='1';
    when "011110" => hec_enable<='1';
    when "011111" => hec_enable<='1';
    when "100000" => hec_enable<='1';
    when "100001" => hec_enable<='1';
    when others => hec_enable<='-';
    
end case;            

case hg_cnt_val is

    when "000000" => hg_done_s<='0';
    when "000001" => hg_done_s<='0';
    when "100010" => hg_done_s<='1';
    when "000010" => hg_done_s<='0';
    when "000011"=> hg_done_s<='0';
    when "000100" => hg_done_s<='0';
    when "000101" => hg_done_s<='0';
    when "000110" => hg_done_s<='0';
    when "000111" => hg_done_s<='0';
    when "001000" => hg_done_s<='0';
    when "001001" => hg_done_s<='0';
    when "001010" => hg_done_s<='0';
    when "001011" => hg_done_s<='0';
    when "001100" => hg_done_s<='0';
    when "001101" => hg_done_s<='0';
    when "001110" => hg_done_s<='0';
    when "001111" => hg_done_s<='0';
    when "010000" => hg_done_s<='0';
    when "010001" => hg_done_s<='0';
    when "010010" => hg_done_s<='0';
    when "010011" => hg_done_s<='0';
    when "010100" => hg_done_s<='0';
    when "010101" => hg_done_s<='0';
    when "010110" => hg_done_s<='0';
    when "010111" => hg_done_s<='0';
    when "011000" => hg_done_s<='0';
    when "011001" => hg_done_s<='0';
    when "011010" => hg_done_s<='0';
    when "011011" => hg_done_s<='0';
    when "011100" => hg_done_s<='0';
    when "011101" => hg_done_s<='0';
    when "011110" => hg_done_s<='0';
    when "011111" => hg_done_s<='0';
    when "100000" => hg_done_s<='0';
    when "100001" => hg_done_s<='0';
    when others => hg_done_s<='-';
    
end case;      

case hg_cnt_val is

    when "000010" => hec_in<=head_intern(31);
    when "000011" => hec_in<=head_intern(30);
    when "000100" => hec_in<=head_intern(29);
    when "000101" => hec_in<=head_intern(28);
    when "000110" => hec_in<=head_intern(27);
    when "000111" => hec_in<=head_intern(26);
    when "001000" => hec_in<=head_intern(25);
    when "001001" => hec_in<=head_intern(24);
    when "001010" => hec_in<=head_intern(23);
    when "001011" => hec_in<=head_intern(22);
    when "001100" => hec_in<=head_intern(21);
    when "001101" => hec_in<=head_intern(20);
    when "001110" => hec_in<=head_intern(19);
    when "001111" => hec_in<=head_intern(18);
    when "010000" => hec_in<=head_intern(17);
    when "010001" => hec_in<=head_intern(16);
    when "010010" => hec_in<=head_intern(15);
    when "010011" => hec_in<=head_intern(14);
    when "010100" => hec_in<=head_intern(13);
    when "010101" => hec_in<=head_intern(12);
    when "010110" => hec_in<=head_intern(11);
    when "010111" => hec_in<=head_intern(10);
    when "011000" => hec_in<=head_intern(9);
    when "011001" => hec_in<=head_intern(8);
    when "011010" => hec_in<=head_intern(7);
    when "011011" => hec_in<=head_intern(6);
    when "011100" => hec_in<=head_intern(5);
    when "011101" => hec_in<=head_intern(4);
    when "011110" => hec_in<=head_intern(3);
    when "011111" => hec_in<=head_intern(2);
    when "100000" => hec_in<=head_intern(1);
    when "100001" => hec_in<=head_intern(0);
    when others => hec_in <= '-';
        
end case;

case out_cnt_val is

    when "010" => head_data<= head_intern(31 downto 24);
    when "011" => head_data<= head_intern(23 downto 16);
    when "100" => head_data<= head_intern(15 downto  8);
    when "101" => head_data<= head_intern( 7 downto  0);
    when "110" => head_data<= hec_val;
    when others => head_data<=(others=>'-');
    
end case;

end process;


HEC_GEN_UNIT: hec_gen PORT map(hec_in, clk, hec_res, hec_enable, hec_val);

OUT_CNT: counter
         generic map ( Bit_Size=> 3, Start=> 1 )
         port map ( clk , output_head, res, "110", out_cnt_val );
          
            
	 
END struct;
