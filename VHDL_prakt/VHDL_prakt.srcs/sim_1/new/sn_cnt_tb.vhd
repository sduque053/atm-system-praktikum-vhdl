library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity sn_cnt_tb is
end sn_cnt_tb;

architecture Behavioral of sn_cnt_tb is

component sn_cnt is
  Port (    clk, res, enbl    :IN  std_logic;
            cnt_0,cnt_1,cnt_2   :OUT std_logic
  );
end component;

component stim_gen IS
    PORT (clk, res, enab : OUT std_logic);
END component;

signal clk,res,enab,c0,c1,c2 :std_logic;

begin

inst1: stim_gen port map(clk,res,enab);
inst2: sn_cnt   port map(clk,res,enab,c0,c1,c2);

end Behavioral;
