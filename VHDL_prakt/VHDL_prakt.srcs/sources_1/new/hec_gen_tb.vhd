library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity hec_gen_tb is
end hec_gen_tb;

architecture Behavioral of hec_gen_tb is
component hec_gen
    Port ( data : in STD_LOGIC;
           clk : in STD_LOGIC;
           res : in STD_LOGIC;
           enab : in STD_LOGIC;
           hec : out STD_LOGIC_VECTOR (7 downto 0));
end component;

signal d,r,e : std_logic := '0';
signal c : std_logic := '1';
signal h : std_logic_vector(7 downto 0);

begin

GEN: hec_gen port map(d,c,r,e,h);


cg: PROCESS        -- Erzeugung Takt
    BEGIN 
	WAIT FOR 400 ns;
	c <= NOT c;
END PROCESS cg;

dg: PROCESS         -- Erzeugung sonstige Stimuli
    BEGIN
	WAIT FOR 800 ns;
	r <= '1';
	WAIT FOR 800 ns;
	r <= '0';
	WAIT FOR 800 ns;
	e <= '1';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '1';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '1';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '1';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '1';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '1';
	WAIT FOR 800 ns;
    d <= '1';
	WAIT FOR 800 ns;
    d <= '1';
	WAIT FOR 800 ns;
    d <= '1';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '1';
	WAIT FOR 800 ns;
    d <= '1';
	WAIT FOR 800 ns;
    d <= '1';
	WAIT FOR 800 ns;
    d <= '1';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '0';
	WAIT FOR 800 ns;
    d <= '0';
    e <= '0';
end process dg;

end Behavioral;
