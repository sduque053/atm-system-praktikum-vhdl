library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity aal1_gen is
    Port ( clk : in STD_LOGIC;
           cnt_enab : in STD_LOGIC;
           res : in STD_LOGIC;
           aal1 : out STD_LOGIC_VECTOR (7 downto 0));
end aal1_gen;

architecture Behavioral of aal1_gen is

COMPONENT sn_cnt port(
            clk, res, enbl    :IN  std_logic;
            cnt_0,cnt_1,cnt_2   :OUT std_logic
);
END COMPONENT;

COMPONENT crc_gen port(
            CSI : IN  std_logic; 
            SN  : IN  std_logic_vector(2 downto 0);
            SNP : OUT std_logic_vector(3 downto 0)
);
END COMPONENT;

SIGNAL sn_temp : std_logic_vector(2 downto 0);
SIGNAL snp: std_logic_vector(3 downto 0);
begin

SN: sn_cnt port map(clk, res, cnt_enab, sn_temp(0), sn_temp(1), sn_temp(2));

CRC: crc_gen port map('0',sn_temp,snp);

aal1<='0'&sn_temp&snp;

end Behavioral;
