library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity ctl_in is
    Port ( clk : in std_logic;
           res : in std_logic;
           hg_done : in STD_LOGIC;
           ready : out STD_LOGIC;
           output_head : out STD_LOGIC;
           inc_aal1 : out STD_LOGIC;
           sel : out STD_LOGIC_VECTOR (1 downto 0);
           we_fifo : out STD_LOGIC);
end ctl_in;

architecture Behavioral of ctl_in is

type state is (init, start, head_wr, aal1_new, aal1_wr, waiting, data_wr, last_bt);
signal cur_state, nex_state: state;
signal data_cnt : std_logic_vector(8 downto 0);
signal data_cnt_2_0 : std_logic_vector(2 downto 0);
signal ready_s : std_logic;

component counter
        generic( Bit_Size : integer := 8;
             Start : integer:= 0);
        Port ( clk : in STD_LOGIC;
               enab : in STD_LOGIC;
               res : in STD_LOGIC;
               target : in std_logic_vector(Bit_Size-1 downto 0);
               cnt_val : out STD_LOGIC_vector(Bit_Size-1 downto 0)
               );
end component;

begin

p1:process(cur_state,hg_done,data_cnt_2_0,data_cnt)

begin

case cur_state is
    when init=>
        if hg_done='1' then
            nex_state<=start;
        else
            nex_state<=init;
        end if;

    when start=>
        nex_state<=head_wr;

    when head_wr=>
        if data_cnt="000000100" then
            nex_state<=aal1_new;
        else
            nex_state<=head_wr;
        end if;
    when aal1_new=>
        nex_state<=aal1_wr;
    when aal1_wr=>
        nex_state<=waiting;
    when waiting=>
        if data_cnt_2_0="111" and data_cnt/="101110111" then
            nex_state<=data_wr;
        elsif data_cnt_2_0="111" and data_cnt="101110111" then
            nex_state<=last_bt;
        else
            nex_state<=waiting;
        end if;
    when data_wr=>
        nex_state<=waiting;
    when last_bt=>
        nex_state<=head_wr;
end case;

case cur_state is
    when init=>
        ready_s<='0';
        output_head<='0';
        inc_aal1<='0';
        sel<="00";
        we_fifo<='0';
        
    when start=>
        ready_s<='1';
        output_head<='1';
        inc_aal1<='0';
        sel<="00";
        we_fifo<='0';

    when head_wr=>
        ready_s<='1';
        output_head<='1';
        inc_aal1<='0';
        sel<="10";
        we_fifo<='1';

    when aal1_new=>
        ready_s<='1';
        output_head<='0';
        inc_aal1<='1';
        sel<="10";
        we_fifo<='1';

    when aal1_wr=>
        ready_s<='1';
        output_head<='0';
        inc_aal1<='0';
        sel<="01";
        we_fifo<='1';
        
    when waiting=>
        ready_s<='1';
        output_head<='0';
        inc_aal1<='0';
        sel<="00";
        we_fifo<='0';

    when data_wr=>
        ready_s<='1';
        output_head<='0';
        inc_aal1<='0';
        sel<="00";
        we_fifo<='1';

    when last_bt=>
        ready_s<='1';
        output_head<='1';
        inc_aal1<='0';
        sel<="00";
        we_fifo<='1';

end case;

end process p1;

p2:process(clk)

begin

if clk'EVENT and clk='1' then
    if res='1' then
        cur_state<=init;
    else
        cur_state<=nex_state;
    end if;
end if;

end process p2;


CNTR: counter generic map( Bit_Size=>9, Start=>0 ) port map ( clk , ready_s, res, "101110111", data_cnt);

ready<=ready_s;
data_cnt_2_0<=data_cnt(2 downto 0);

end Behavioral;
