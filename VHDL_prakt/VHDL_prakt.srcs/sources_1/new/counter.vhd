library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity counter is
    generic( Bit_Size : integer := 8;
             Start : integer:= 0);
    Port ( clk : in STD_LOGIC;
           enab : in STD_LOGIC;
           res : in STD_LOGIC;
           target : in std_logic_vector(Bit_Size-1 downto 0);
           cnt_val : out STD_LOGIC_vector(Bit_Size-1 downto 0)
           );
end counter;

architecture Behavioral of counter is

signal cnt_out, cnt_max : integer range 0 to Bit_Size-1 :=0;

begin

cnt_max <= to_integer( unsigned (target));

process(clk)

begin

    if clk'EVENT and clk='1' then
        if res='1' then
            
            cnt_out<= Start;
            
        elsif cnt_out=cnt_max and enab='1' then
        
            cnt_out<= Start; 
               
        elsif enab='1' then 
        
            cnt_out <= (cnt_out + 1);

        end if;
    
    end if;
   
end process;

cnt_val <= std_logic_vector(to_unsigned(cnt_out, cnt_val'LENGTH));
       
end Behavioral;
