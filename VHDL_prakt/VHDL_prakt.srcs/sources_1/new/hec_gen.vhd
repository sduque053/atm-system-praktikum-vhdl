library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity hec_gen is
    Port ( data : in STD_LOGIC;
           clk : in STD_LOGIC;
           res : in STD_LOGIC;
           enab : in STD_LOGIC;
           hec : out STD_LOGIC_VECTOR (7 downto 0));
end hec_gen;

architecture Behavioral of hec_gen is

constant xor_constant : std_logic_vector(7 downto 0) := x"55";

signal Q_out, D_in : std_logic_vector(7 downto 0);

component my_dff_re
    PORT (d, clk, en, res : IN std_logic;
	  q, qb  : OUT std_logic);
END component ;
	
begin

GEN_FF_XOR: for I in 0 to 7 generate

    DFFI: my_dff_re port map(
        D_in(I), clk, enab, res,
        Q_out(I), open );
        
    XORI: hec(I)<=Q_out(I) xor xor_constant(I);
    
    GEN_CONN: if I > 2 generate
        D_in(I)<=Q_out(I-1);
    end generate GEN_CONN;
    
end generate GEN_FF_XOR;

D_in(0)<=data xor Q_out(7);
D_in(1)<=D_in(0) xor Q_out(0);
D_in(2)<=D_in(0) xor Q_out(1);

end Behavioral;
