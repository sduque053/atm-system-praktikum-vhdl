library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity sn_cnt is
  Port (    clk, res, enbl    :IN  std_logic;
            cnt_0,cnt_1,cnt_2   :OUT std_logic
  );
end sn_cnt;

architecture Behavioral of sn_cnt is

SIGNAL ff1q, ff1qn, a1, a2, o1, ff2q, ff2qn, an1, a3, a41, a42, na3, a5, o2, ff3q, ff3qn : std_logic;

COMPONENT my_and2 IS
    PORT (a, b : IN std_logic;
          y : OUT std_logic);
END COMPONENT;

COMPONENT my_or2 IS
    PORT (a, b : IN std_logic;
          y : OUT std_logic);
END COMPONENT;

COMPONENT my_dff_re IS
    PORT (  d, clk, en, res : IN std_logic;
	        q, qb  : OUT std_logic);
END COMPONENT;

begin

ff1: my_dff_re PORT MAP(ff1qn,clk,enbl,res,ff1q,ff1qn);
ff2: my_dff_re PORT MAP(o1,clk,enbl,res,ff2q,ff2qn);
ff3: my_dff_re PORT MAP(o2,clk,enbl,res,ff3q,ff3qn);

and1: my_and2 PORT MAP(ff1q,ff2qn,a1);
and2: my_and2 PORT MAP(ff1qn,ff2q,a2);
and3: my_and2 PORT MAP(ff2q,ff1q,a3);
na3<=not a3;
and41:my_and2 PORT MAP(ff1q,ff2q,a41);
and42:my_and2 PORT MAP(a41,ff3qn,a42);
and5: my_and2 PORT MAP(ff3q,na3,a5);

or1: my_or2 PORT MAP(a1,a2,o1);
or2: my_or2 PORT MAP(a5,a42,o2);

cnt_2<=ff3q;
cnt_1<=ff2q;
cnt_0<=ff1q;

end Behavioral;
