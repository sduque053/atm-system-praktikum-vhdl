library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity crc_gen is
  Port (
            CSI : IN  std_logic; 
            SN  : IN  std_logic_vector(2 downto 0);
            SNP : OUT std_logic_vector(3 downto 0)
  );
end crc_gen;

architecture Behavioral of crc_gen is

SIGNAL TMP1, TMP2, TMP3, TMP4 : std_logic;
SIGNAL SNP_T : std_logic_vector(3 downto 0);

begin

SNP_T(0)<= CSI xor SN(2) xor SN(1) xor SN(0) xor SNP_T(3) xor SNP_T(2) xor SNP_T(1);
SNP_T(1)<= SN(2) xor SN(0);

TMP1<= SN(2) and SN(1) and SN(0);
TMP2<= SN(2) and not SN(1) and not SN(0);
TMP3<= not SN(2) and SN(1) and not SN(0);
TMP4<= not SN(2) and not SN(1) and SN(0);

SNP_T(2)<= TMP1 or TMP2 or TMP3 or TMP4;

SNP_T(3)<= SN(2) xor SN(1);

SNP<=SNP_T;

end Behavioral;
