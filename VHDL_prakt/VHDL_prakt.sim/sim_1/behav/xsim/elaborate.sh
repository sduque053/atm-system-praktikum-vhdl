#!/bin/bash -f
# ****************************************************************************
# Vivado (TM) v2019.1 (64-bit)
#
# Filename    : elaborate.sh
# Simulator   : Xilinx Vivado Simulator
# Description : Script for elaborating the compiled design
#
# Generated by Vivado on Wed Apr 21 13:12:33 CEST 2021
# SW Build 2552052 on Fri May 24 14:47:09 MDT 2019
#
# Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
#
# usage: elaborate.sh
#
# ****************************************************************************
set -Eeuo pipefail
echo "xelab -wto 5b9145d45fa5443fb63603be468aa365 --incr --debug typical --relax --mt 8 -L xil_defaultlib -L secureip --snapshot sn_cnt_tb_behav xil_defaultlib.sn_cnt_tb -log elaborate.log"
xelab -wto 5b9145d45fa5443fb63603be468aa365 --incr --debug typical --relax --mt 8 -L xil_defaultlib -L secureip --snapshot sn_cnt_tb_behav xil_defaultlib.sn_cnt_tb -log elaborate.log

