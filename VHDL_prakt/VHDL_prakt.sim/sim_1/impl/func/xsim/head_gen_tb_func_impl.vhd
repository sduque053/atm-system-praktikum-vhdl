-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
-- Date        : Fri Jun 11 16:58:18 2021
-- Host        : DESKTOP-7658P21 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               D:/Uni/Austausch_2/VHDL/Praktikum_VHDL/VHDL_prakt/VHDL_prakt.sim/sim_1/impl/func/xsim/head_gen_tb_func_impl.vhd
-- Design      : head_gen
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \counter__parameterized1\ is
  port (
    \cnt_out_reg[0]_0\ : out STD_LOGIC;
    output_head_IBUF : in STD_LOGIC;
    res_IBUF : in STD_LOGIC;
    d_clk_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \counter__parameterized1\ : entity is "counter";
end \counter__parameterized1\;

architecture STRUCTURE of \counter__parameterized1\ is
  signal \cnt_out[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_out[1]_i_1_n_0\ : STD_LOGIC;
  signal \^cnt_out_reg[0]_0\ : STD_LOGIC;
  signal \cnt_out_reg_n_0_[1]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \cnt_out[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \cnt_out[1]_i_1\ : label is "soft_lutpair0";
begin
  \cnt_out_reg[0]_0\ <= \^cnt_out_reg[0]_0\;
\cnt_out[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF5E"
    )
        port map (
      I0 => output_head_IBUF,
      I1 => \cnt_out_reg_n_0_[1]\,
      I2 => \^cnt_out_reg[0]_0\,
      I3 => res_IBUF,
      O => \cnt_out[0]_i_1_n_0\
    );
\cnt_out[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0060"
    )
        port map (
      I0 => output_head_IBUF,
      I1 => \cnt_out_reg_n_0_[1]\,
      I2 => \^cnt_out_reg[0]_0\,
      I3 => res_IBUF,
      O => \cnt_out[1]_i_1_n_0\
    );
\cnt_out_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => '1',
      D => \cnt_out[0]_i_1_n_0\,
      Q => \^cnt_out_reg[0]_0\,
      R => '0'
    );
\cnt_out_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => '1',
      D => \cnt_out[1]_i_1_n_0\,
      Q => \cnt_out_reg_n_0_[1]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re is
  port (
    head_data_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC;
    \head_data[0]\ : in STD_LOGIC;
    data0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end my_dff_re;

architecture STRUCTURE of my_dff_re is
  signal data1 : STD_LOGIC_VECTOR ( 0 to 0 );
begin
\head_data_OBUF[0]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(0),
      I1 => \head_data[0]\,
      I2 => data0(0),
      O => head_data_OBUF(0)
    );
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data1(0),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re_0 is
  port (
    head_data_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC;
    \head_data[1]\ : in STD_LOGIC;
    data0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of my_dff_re_0 : entity is "my_dff_re";
end my_dff_re_0;

architecture STRUCTURE of my_dff_re_0 is
  signal data1 : STD_LOGIC_VECTOR ( 1 to 1 );
begin
\head_data_OBUF[1]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(1),
      I1 => \head_data[1]\,
      I2 => data0(0),
      O => head_data_OBUF(0)
    );
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data1(1),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re_1 is
  port (
    head_data_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC;
    \head_data[2]\ : in STD_LOGIC;
    data0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of my_dff_re_1 : entity is "my_dff_re";
end my_dff_re_1;

architecture STRUCTURE of my_dff_re_1 is
  signal data1 : STD_LOGIC_VECTOR ( 2 to 2 );
begin
\head_data_OBUF[2]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(2),
      I1 => \head_data[2]\,
      I2 => data0(0),
      O => head_data_OBUF(0)
    );
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data1(2),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re_10 is
  port (
    data0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of my_dff_re_10 : entity is "my_dff_re";
end my_dff_re_10;

architecture STRUCTURE of my_dff_re_10 is
begin
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data0(0),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re_11 is
  port (
    data0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of my_dff_re_11 : entity is "my_dff_re";
end my_dff_re_11;

architecture STRUCTURE of my_dff_re_11 is
begin
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data0(0),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re_12 is
  port (
    data0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of my_dff_re_12 : entity is "my_dff_re";
end my_dff_re_12;

architecture STRUCTURE of my_dff_re_12 is
begin
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data0(0),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re_13 is
  port (
    data0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of my_dff_re_13 : entity is "my_dff_re";
end my_dff_re_13;

architecture STRUCTURE of my_dff_re_13 is
begin
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data0(0),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re_14 is
  port (
    data0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of my_dff_re_14 : entity is "my_dff_re";
end my_dff_re_14;

architecture STRUCTURE of my_dff_re_14 is
begin
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data0(0),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re_2 is
  port (
    head_data_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC;
    \head_data[3]\ : in STD_LOGIC;
    data0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of my_dff_re_2 : entity is "my_dff_re";
end my_dff_re_2;

architecture STRUCTURE of my_dff_re_2 is
  signal data1 : STD_LOGIC_VECTOR ( 3 to 3 );
begin
\head_data_OBUF[3]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(3),
      I1 => \head_data[3]\,
      I2 => data0(0),
      O => head_data_OBUF(0)
    );
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data1(3),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re_3 is
  port (
    head_data_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC;
    \head_data[4]\ : in STD_LOGIC;
    data0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of my_dff_re_3 : entity is "my_dff_re";
end my_dff_re_3;

architecture STRUCTURE of my_dff_re_3 is
  signal data1 : STD_LOGIC_VECTOR ( 4 to 4 );
begin
\head_data_OBUF[4]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(4),
      I1 => \head_data[4]\,
      I2 => data0(0),
      O => head_data_OBUF(0)
    );
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data1(4),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re_4 is
  port (
    head_data_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC;
    \head_data[5]\ : in STD_LOGIC;
    data0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of my_dff_re_4 : entity is "my_dff_re";
end my_dff_re_4;

architecture STRUCTURE of my_dff_re_4 is
  signal data1 : STD_LOGIC_VECTOR ( 5 to 5 );
begin
\head_data_OBUF[5]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(5),
      I1 => \head_data[5]\,
      I2 => data0(0),
      O => head_data_OBUF(0)
    );
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data1(5),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re_5 is
  port (
    head_data_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC;
    \head_data[6]\ : in STD_LOGIC;
    data0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of my_dff_re_5 : entity is "my_dff_re";
end my_dff_re_5;

architecture STRUCTURE of my_dff_re_5 is
  signal data1 : STD_LOGIC_VECTOR ( 6 to 6 );
begin
\head_data_OBUF[6]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(6),
      I1 => \head_data[6]\,
      I2 => data0(0),
      O => head_data_OBUF(0)
    );
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data1(6),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re_6 is
  port (
    head_data_OBUF : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC;
    \head_data[7]\ : in STD_LOGIC;
    data0 : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of my_dff_re_6 : entity is "my_dff_re";
end my_dff_re_6;

architecture STRUCTURE of my_dff_re_6 is
  signal data1 : STD_LOGIC_VECTOR ( 7 to 7 );
begin
\head_data_OBUF[7]_inst_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => data1(7),
      I1 => \head_data[7]\,
      I2 => data0(0),
      O => head_data_OBUF(0)
    );
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data1(7),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re_7 is
  port (
    data0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of my_dff_re_7 : entity is "my_dff_re";
end my_dff_re_7;

architecture STRUCTURE of my_dff_re_7 is
begin
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data0(0),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re_8 is
  port (
    data0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of my_dff_re_8 : entity is "my_dff_re";
end my_dff_re_8;

architecture STRUCTURE of my_dff_re_8 is
begin
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data0(0),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity my_dff_re_9 is
  port (
    data0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    res_IBUF : in STD_LOGIC;
    set_head_IBUF : in STD_LOGIC;
    head_in_IBUF : in STD_LOGIC_VECTOR ( 0 to 0 );
    d_clk_IBUF_BUFG : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of my_dff_re_9 : entity is "my_dff_re";
end my_dff_re_9;

architecture STRUCTURE of my_dff_re_9 is
begin
q_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => d_clk_IBUF_BUFG,
      CE => set_head_IBUF,
      D => head_in_IBUF(0),
      Q => data0(0),
      R => res_IBUF
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity head_gen is
  port (
    d_clk : in STD_LOGIC;
    res : in STD_LOGIC;
    set_head : in STD_LOGIC;
    head_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    output_head : in STD_LOGIC;
    head_data : out STD_LOGIC_VECTOR ( 7 downto 0 );
    hg_done : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of head_gen : entity is true;
  attribute ECO_CHECKSUM : string;
  attribute ECO_CHECKSUM of head_gen : entity is "6d20e64f";
end head_gen;

architecture STRUCTURE of head_gen is
  signal OUT_CNT_n_0 : STD_LOGIC;
  signal d_clk_IBUF : STD_LOGIC;
  signal d_clk_IBUF_BUFG : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal head_data_OBUF : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal head_in_IBUF : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal output_head_IBUF : STD_LOGIC;
  signal res_IBUF : STD_LOGIC;
  signal set_head_IBUF : STD_LOGIC;
begin
\GEN_HREG[16].H_REG\: entity work.my_dff_re
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(0),
      \head_data[0]\ => OUT_CNT_n_0,
      head_data_OBUF(0) => head_data_OBUF(0),
      head_in_IBUF(0) => head_in_IBUF(16),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
\GEN_HREG[17].H_REG\: entity work.my_dff_re_0
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(1),
      \head_data[1]\ => OUT_CNT_n_0,
      head_data_OBUF(0) => head_data_OBUF(1),
      head_in_IBUF(0) => head_in_IBUF(17),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
\GEN_HREG[18].H_REG\: entity work.my_dff_re_1
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(2),
      \head_data[2]\ => OUT_CNT_n_0,
      head_data_OBUF(0) => head_data_OBUF(2),
      head_in_IBUF(0) => head_in_IBUF(18),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
\GEN_HREG[19].H_REG\: entity work.my_dff_re_2
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(3),
      \head_data[3]\ => OUT_CNT_n_0,
      head_data_OBUF(0) => head_data_OBUF(3),
      head_in_IBUF(0) => head_in_IBUF(19),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
\GEN_HREG[20].H_REG\: entity work.my_dff_re_3
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(4),
      \head_data[4]\ => OUT_CNT_n_0,
      head_data_OBUF(0) => head_data_OBUF(4),
      head_in_IBUF(0) => head_in_IBUF(20),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
\GEN_HREG[21].H_REG\: entity work.my_dff_re_4
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(5),
      \head_data[5]\ => OUT_CNT_n_0,
      head_data_OBUF(0) => head_data_OBUF(5),
      head_in_IBUF(0) => head_in_IBUF(21),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
\GEN_HREG[22].H_REG\: entity work.my_dff_re_5
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(6),
      \head_data[6]\ => OUT_CNT_n_0,
      head_data_OBUF(0) => head_data_OBUF(6),
      head_in_IBUF(0) => head_in_IBUF(22),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
\GEN_HREG[23].H_REG\: entity work.my_dff_re_6
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(7),
      \head_data[7]\ => OUT_CNT_n_0,
      head_data_OBUF(0) => head_data_OBUF(7),
      head_in_IBUF(0) => head_in_IBUF(23),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
\GEN_HREG[24].H_REG\: entity work.my_dff_re_7
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(0),
      head_in_IBUF(0) => head_in_IBUF(24),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
\GEN_HREG[25].H_REG\: entity work.my_dff_re_8
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(1),
      head_in_IBUF(0) => head_in_IBUF(25),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
\GEN_HREG[26].H_REG\: entity work.my_dff_re_9
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(2),
      head_in_IBUF(0) => head_in_IBUF(26),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
\GEN_HREG[27].H_REG\: entity work.my_dff_re_10
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(3),
      head_in_IBUF(0) => head_in_IBUF(27),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
\GEN_HREG[28].H_REG\: entity work.my_dff_re_11
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(4),
      head_in_IBUF(0) => head_in_IBUF(28),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
\GEN_HREG[29].H_REG\: entity work.my_dff_re_12
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(5),
      head_in_IBUF(0) => head_in_IBUF(29),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
\GEN_HREG[30].H_REG\: entity work.my_dff_re_13
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(6),
      head_in_IBUF(0) => head_in_IBUF(30),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
\GEN_HREG[31].H_REG\: entity work.my_dff_re_14
     port map (
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      data0(0) => data0(7),
      head_in_IBUF(0) => head_in_IBUF(31),
      res_IBUF => res_IBUF,
      set_head_IBUF => set_head_IBUF
    );
OUT_CNT: entity work.\counter__parameterized1\
     port map (
      \cnt_out_reg[0]_0\ => OUT_CNT_n_0,
      d_clk_IBUF_BUFG => d_clk_IBUF_BUFG,
      output_head_IBUF => output_head_IBUF,
      res_IBUF => res_IBUF
    );
d_clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => d_clk_IBUF,
      O => d_clk_IBUF_BUFG
    );
d_clk_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => d_clk,
      O => d_clk_IBUF
    );
\head_data_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => head_data_OBUF(0),
      O => head_data(0)
    );
\head_data_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => head_data_OBUF(1),
      O => head_data(1)
    );
\head_data_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => head_data_OBUF(2),
      O => head_data(2)
    );
\head_data_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => head_data_OBUF(3),
      O => head_data(3)
    );
\head_data_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => head_data_OBUF(4),
      O => head_data(4)
    );
\head_data_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => head_data_OBUF(5),
      O => head_data(5)
    );
\head_data_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => head_data_OBUF(6),
      O => head_data(6)
    );
\head_data_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => head_data_OBUF(7),
      O => head_data(7)
    );
\head_in_IBUF[16]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(16),
      O => head_in_IBUF(16)
    );
\head_in_IBUF[17]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(17),
      O => head_in_IBUF(17)
    );
\head_in_IBUF[18]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(18),
      O => head_in_IBUF(18)
    );
\head_in_IBUF[19]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(19),
      O => head_in_IBUF(19)
    );
\head_in_IBUF[20]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(20),
      O => head_in_IBUF(20)
    );
\head_in_IBUF[21]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(21),
      O => head_in_IBUF(21)
    );
\head_in_IBUF[22]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(22),
      O => head_in_IBUF(22)
    );
\head_in_IBUF[23]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(23),
      O => head_in_IBUF(23)
    );
\head_in_IBUF[24]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(24),
      O => head_in_IBUF(24)
    );
\head_in_IBUF[25]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(25),
      O => head_in_IBUF(25)
    );
\head_in_IBUF[26]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(26),
      O => head_in_IBUF(26)
    );
\head_in_IBUF[27]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(27),
      O => head_in_IBUF(27)
    );
\head_in_IBUF[28]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(28),
      O => head_in_IBUF(28)
    );
\head_in_IBUF[29]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(29),
      O => head_in_IBUF(29)
    );
\head_in_IBUF[30]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(30),
      O => head_in_IBUF(30)
    );
\head_in_IBUF[31]_inst\: unisim.vcomponents.IBUF
     port map (
      I => head_in(31),
      O => head_in_IBUF(31)
    );
hg_done_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => hg_done
    );
output_head_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => output_head,
      O => output_head_IBUF
    );
res_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => res,
      O => res_IBUF
    );
set_head_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => set_head,
      O => set_head_IBUF
    );
end STRUCTURE;
