-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
-- Date        : Thu Jun 10 21:10:37 2021
-- Host        : DESKTOP-7658P21 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               D:/Uni/Austausch_2/VHDL/Praktikum_VHDL/VHDL_prakt/VHDL_prakt.sim/sim_1/impl/func/xsim/counter_func_impl.vhd
-- Design      : counter
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity counter is
  port (
    enab : in STD_LOGIC;
    reset : in STD_LOGIC;
    clk : in STD_LOGIC;
    target : in STD_LOGIC_VECTOR ( 7 downto 0 );
    cnt_val : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of counter : entity is true;
  attribute Bit_Size : integer;
  attribute Bit_Size of counter : entity is 8;
  attribute ECO_CHECKSUM : string;
  attribute ECO_CHECKSUM of counter : entity is "c406d14e";
end counter;

architecture STRUCTURE of counter is
  signal clk_IBUF : STD_LOGIC;
  signal clk_IBUF_BUFG : STD_LOGIC;
  signal \cnt_out[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_out[1]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_out[1]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_out[1]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_out[2]_i_1_n_0\ : STD_LOGIC;
  signal cnt_val_OBUF : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal p_0_in : STD_LOGIC_VECTOR ( 2 to 2 );
  signal reset_IBUF : STD_LOGIC;
  signal target_IBUF : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \cnt_out[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \cnt_out[2]_i_1\ : label is "soft_lutpair0";
begin
clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_IBUF,
      O => clk_IBUF_BUFG
    );
clk_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => clk,
      O => clk_IBUF
    );
\cnt_out[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0093"
    )
        port map (
      I0 => target_IBUF(0),
      I1 => cnt_val_OBUF(0),
      I2 => \cnt_out[1]_i_3_n_0\,
      I3 => reset_IBUF,
      O => \cnt_out[0]_i_1_n_0\
    );
\cnt_out[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FF008778"
    )
        port map (
      I0 => cnt_val_OBUF(0),
      I1 => target_IBUF(0),
      I2 => target_IBUF(1),
      I3 => \cnt_out[1]_i_2_n_0\,
      I4 => \cnt_out[1]_i_3_n_0\,
      I5 => reset_IBUF,
      O => \cnt_out[1]_i_1_n_0\
    );
\cnt_out[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3C3C9696DA3A9696"
    )
        port map (
      I0 => cnt_val_OBUF(0),
      I1 => cnt_val_OBUF(1),
      I2 => target_IBUF(1),
      I3 => cnt_val_OBUF(2),
      I4 => target_IBUF(0),
      I5 => target_IBUF(2),
      O => \cnt_out[1]_i_2_n_0\
    );
\cnt_out[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A4B5314093F5B1F5"
    )
        port map (
      I0 => target_IBUF(2),
      I1 => target_IBUF(0),
      I2 => cnt_val_OBUF(2),
      I3 => cnt_val_OBUF(0),
      I4 => cnt_val_OBUF(1),
      I5 => target_IBUF(1),
      O => \cnt_out[1]_i_3_n_0\
    );
\cnt_out[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => p_0_in(2),
      I1 => reset_IBUF,
      O => \cnt_out[2]_i_1_n_0\
    );
\cnt_out[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0A0080A009101010"
    )
        port map (
      I0 => target_IBUF(2),
      I1 => target_IBUF(0),
      I2 => cnt_val_OBUF(2),
      I3 => cnt_val_OBUF(0),
      I4 => cnt_val_OBUF(1),
      I5 => target_IBUF(1),
      O => p_0_in(2)
    );
\cnt_out_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \cnt_out[0]_i_1_n_0\,
      Q => cnt_val_OBUF(0),
      R => '0'
    );
\cnt_out_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \cnt_out[1]_i_1_n_0\,
      Q => cnt_val_OBUF(1),
      R => '0'
    );
\cnt_out_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \cnt_out[2]_i_1_n_0\,
      Q => cnt_val_OBUF(2),
      R => '0'
    );
\cnt_val_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cnt_val_OBUF(0),
      O => cnt_val(0)
    );
\cnt_val_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cnt_val_OBUF(1),
      O => cnt_val(1)
    );
\cnt_val_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => cnt_val_OBUF(2),
      O => cnt_val(2)
    );
\cnt_val_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => cnt_val(3)
    );
\cnt_val_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => cnt_val(4)
    );
\cnt_val_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => cnt_val(5)
    );
\cnt_val_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => cnt_val(6)
    );
\cnt_val_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '0',
      O => cnt_val(7)
    );
reset_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => reset,
      O => reset_IBUF
    );
\target_IBUF[0]_inst\: unisim.vcomponents.IBUF
     port map (
      I => target(0),
      O => target_IBUF(0)
    );
\target_IBUF[1]_inst\: unisim.vcomponents.IBUF
     port map (
      I => target(1),
      O => target_IBUF(1)
    );
\target_IBUF[2]_inst\: unisim.vcomponents.IBUF
     port map (
      I => target(2),
      O => target_IBUF(2)
    );
end STRUCTURE;
